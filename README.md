# NATIONAL HOLIDAY WEB APP #

This web app allow you to check whether the date you entered is an Indonesian National Holiday or not.

### Setup ###

* Install required php modules `composer install`
* Install required node modules `npm install`
* Run `npm run prod` to build frontend assets
* get CLIENT_ID, CLIENT_SECRET and REDIRECT_URI (from Google API Console Oauth 2.0)
* set CLIENT_ID, CLIENT_SECRET and REDIRECT_URI to .env file or leave it to default setting (see .env.example)
* Run `php artisan serve` to run web app at port 8000

### Unit Test ###
* Open page `hostname/unit-test` (http://localhost:8000/unit-test)
