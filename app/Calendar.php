<?php

namespace App;

use \GuzzleHttp\Client;
use \GuzzleHttp\Exception\ClientException;

class Calendar
{
    public const EVENT_URL = 'https://www.googleapis.com/calendar/v3/calendars/en.indonesian%23holiday%40group.v.calendar.google.com/events';

    public static function all($access_token) 
    {
        $client = new Client();
        try {
          $response = $client->request('GET', Calendar::EVENT_URL, [
            'headers' => [
              'Authorization' => 'Bearer '. $access_token  
            ]
          ]);
          $content = (string)$response->getBody();
          return json_decode($content); 
        } catch (ClientException $e) {
          return false;
        }
    }

    public static function insertRequiredCalendar()
    {
        $client = new Client();
        try {
          $client->request('POST', Calendar::EVENT_URL, [
            'form_params' => [
              'id' => 'en.indonesian#holiday@group.v.calendar.google.com'
            ]
          ]);
        } catch (ClientException $e) {
          return false;
        }
    
    }

    public static function authUrl() {
        $authUrl = 'https://accounts.google.com/o/oauth2/v2/auth';
        $authUrl .= '?scope=https://www.googleapis.com/auth/calendar';
        $authUrl .= '&access_type=offline';
        $authUrl .= '&redirect_uri=' . ENV('REDIRECT_URI');
        $authUrl .= '&response_type=code';
        $authUrl .= '&client_id=197861876702-nvdt0a1au5e86ruh7ogq7v0kqusus77u.apps.googleusercontent.com';
        return $authUrl;
    }

    public static function accessToken($code) {
        $client = new Client();
        $form_params = [
          'code' => $code,
          'client_id' => env('CLIENT_ID'),
          'client_secret' => env('CLIENT_SECRET'),
          'redirect_uri' => env('REDIRECT_URI'),
          'grant_type' => 'authorization_code'
        ];
        try {
          $response = $client->request('POST', 'https://www.googleapis.com/oauth2/v4/token', compact('form_params'));
          $content = (string)$response->getBody();
          return json_decode($content); 
        } catch (ClientException $e) {
          return false;
        }
    }
}
