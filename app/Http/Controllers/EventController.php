<?php

namespace App\Http\Controllers;

use App\Calendar;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function auth(Request $request)
    {
        $code = $request->input('code', '');
        if (!$code) {
          return redirect('/');
        }
        $result = Calendar::accessToken($code);
        if ($result === false || !isset($result->access_token)) {
          return redirect('/');
        }
        $access_token = $result->access_token;
        $request->session()->put('access_token', $access_token);
        return redirect('/'); 
    }

    public function events(Request $request)
    {
        $access_token = $request->session()->get('access_token');
        if (!$access_token) {
            return Calendar::authUrl(); 
        }    
        $events = Calendar::all($access_token);
        if ($events === false) {
            return Calendar::authUrl(); 
        }
        
        if (count($events) == 0) {
            $success = Calendar::insertRequiredCalendar(); 
            $events = Calendar::all($access_token);
            if ($success == false || $events === false) {
                return Calendar::authUrl(); 
            }
        }
        return response()->json($events);
    }

    public function form(Request $request)
    {
        return view('event_form');
    }
}
