require('./bootstrap');

window.fetchEvents = (url, callback) => {
  $.ajax(url, {
    success: function(response){
      var events = {};
      if (response.items) {
        $("#input_date").removeAttr("disabled");
        $("#loading").addClass('hidden');
        $("#info").removeClass('hidden');
        for (var i in response.items) {
          var item = response.items[i];
          var parts = item.start.date.split("-");
          var month = parts[1];
          var date = parts[2];
          var key = month + "-" + date;
          events[key] = item.summary;
        }
        callback(events);
      } else {
        location.href = response;
      }
    }
  });
};
