<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>National Holiday</title>
        <link href="{{ elixir('css/app.css') }}" rel="stylesheet"/>
    </head>
    <body>
      <div class="well shadowed">
         <div class="title">
            National Holiday
         </div>
         <div class="form-group">
            <input type="date" id="input_date" class="form-control input-lg" style="width: auto; margin: 0px auto" disabled/>
         </div>
         <div id="loading" class="alert alert-warning" style="text-align: center">
            Please wait, fetching data from server...
         </div>
         <div id="info" class="alert alert-info hidden" style="text-align: center">
            Please enter valid date to check national holiday
         </div>
         <div id="info_correct" class="alert alert-success hidden" style="text-align: center">
         </div>
         <div id="info_incorrect" class="alert alert-danger hidden" style="text-align: center">
         </div>
      </div>
      
      <script src="{{ elixir('js/app.js') }}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
      <script>
        window.events = {};
        window.fetchEvents("{{ url('/events') }}", function(events){
          $("#input_date").removeAttr("disabled");
          $("#loading").addClass('hidden');
          $("#info").removeClass('hidden');
          window.events = events;
        });
        $(function(){
          $("#input_date").change(function(e){
            var value = e.target.value;
            var parts = value.split("-");
            if (parts.length == 3) {
              var month = parts[1];
              var date = parts[2];
              var key = month + "-" + date;
              var date = moment(value);
              if (events[key]) {
                var label = date.format('D MMMM YYYY') + " is " + events[key];
                $("#info").addClass("hidden");
                $("#info_incorrect").addClass("hidden");
                $("#info_correct").html(label).removeClass("hidden");
              } else {
                var label = 'There is no national holiday in ' + date.format('D MMMM YYYY');
                $("#info").addClass("hidden");
                $("#info_correct").addClass("hidden");
                $("#info_incorrect").html(label).removeClass("hidden");
              }
            }
          });
        });
      </script>
    </body>
</html>
