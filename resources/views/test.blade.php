<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Unit Test - National Holiday</title>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jasmine/2.8.0/jasmine.min.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jasmine/2.8.0/jasmine.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jasmine/2.8.0/jasmine-html.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jasmine/2.8.0/boot.min.js"></script>
     </head>
    <body>
      <script src="{{ elixir('js/app.js') }}"></script>
      <script>
        var requestTimeout = 20000; // 20 seconds
        describe("fetchEvents", function() {
          var events = {};
          beforeAll(function(done){
            window.fetchEvents("{{ url('/events') }}", function(result){
              events = result;
              done();
            });
          });

          it("return all events", function() {
            expect(Object.keys(events).length).toBe(54);
          }, requestTimeout);

          it("return valid holiday", function() {
            expect(events["08-17"]).toBe("Indonesian Independence Day");
            expect(events["01-01"]).toBe("New Year's Day");
          }, requestTimeout);
        }); 
      </script>
    </body>
</html>
